{-------------------------------}
 -- Lambda's XMonad Config -----
{-------------------------------}

import XMonad
import XMonad.Operations

import XMonad.Actions.SpawnOn
import XMonad.Actions.CycleWS

import XMonad.ManageHook
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.InsertPosition

import XMonad.Layout.NoBorders
import XMonad.Layout.Fullscreen
import XMonad.Layout.Grid
import XMonad.Layout.Spacing
import XMonad.Layout.Gaps
import XMonad.Layout.BinarySpacePartition

import XMonad.Prompt
import XMonad.Prompt.ConfirmPrompt

import XMonad.Util.Run(spawnPipe, runProcessWithInput)

import qualified XMonad.StackSet as W
import qualified Data.Map as M

import Data.Function
import Data.List

import Control.Monad

import System.IO
import System.Exit

import Graphics.X11.Types
import Graphics.X11.ExtraTypes.XF86

import System.Posix.Signals (signalProcess)
import System.Posix.Types (ProcessID)

altMask = mod1Mask

withPid :: Window -> (ProcessID -> X ()) -> X ()
withPid w f = do
  maybePid <- runProcessWithInput "xcb-client-id" [show w] ""
  maybePid <- if null maybePid
              then runQuery pid w
              else return $ Just $ read maybePid
  whenJust maybePid f

-- send sigkill to window
sigKillWindow :: Window -> X ()
sigKillWindow w = withWindowSet $ \ws ->
    when (W.member w ws) $ withPid w (io . signalProcess 9)

-- safely kill a window
safeKillWindow :: Window -> X ()
safeKillWindow w = withWindowSet $ \ws ->
    when (W.member w ws) $ killWindow w

sigKill :: X ()
sigKill = withFocused sigKillWindow

safeKill :: X ()
safeKill = withFocused safeKillWindow

runLauncher :: String
runLauncher = "rofi -modi combi -combi-modi window,run,drun,ssh \
        \ -font \"Iosevka Term SS05 Regular 14\" -show combi opacity 70"

mailClient :: String
mailClient = "tutanota-desktop-linux.AppImage"

uiFont :: String
uiFont = "xft:Routed Gothic:style=Regular:antialias=true"

{- A custom exit prompt -}
data MyEnterPrompt = MyEnterPrompt String
instance XPrompt MyEnterPrompt where
    showXPrompt (MyEnterPrompt n) = ""

myConfirmPrompt :: XPConfig -> String -> X () -> X ()
myConfirmPrompt config app func = mkXPrompt (MyEnterPrompt app) config
    (mkComplFunFromList []) $ const func

{- honestly i don't know what this does -}
xmobarEscape = concatMap doubleLts
    where doubleLts '<' = "<<"
          doubleLts x   = [x]

{- Workspaces with clickable numbers -}
myWorkspaces :: [String]
myWorkspaces = clickable . (map xmobarEscape) $ ["1", "2", "3", "4", "5", "6",
                                                 "7", "8", "9"]
    where
        clickable l = [ "<action=xdotool key Super+" ++ show (n) ++ ">" ++ ws
                            ++ "</action>" | (i,ws) <- zip [1..9] l,
                                            let n = i]

{- XPrompt Appearance -}
myXPConfig = def
    { font = uiFont ++ ":size=14"
    , bgColor = "#8700ff"
    , fgColor = "#ffffff"
    , fgHLight = "#8700ff"
    , bgHLight = "#ffffff"
    , height = 50
    , promptBorderWidth = 0
    , defaultText = ""
    , position = CenteredAt 0.5 0.5
    , alwaysHighlight = True
    }

{- Custom Keybindings -}
myKeys (XConfig {XMonad.modMask = modm}) = M.fromList $
    [
    -- run launcher
    ((modm, xK_p), spawn runLauncher)
    -- screenshot tool
    , ((modm, xK_Print), spawn "flameshot gui")
    -- xscreensaver lock
    , ((modm .|. shiftMask, xK_l), spawn "xscreensaver-command -lock")


    -- window management functions --
    , ((modm, xK_q), safeKill) -- kill a window
    , ((modm .|. altMask, xK_q), myConfirmPrompt myXPConfig "Really send SIGKILL to window in focus?" $ sigKill) -- safely kill a window
    , ((modm, xK_r), restart "xmonad" True) -- restart XMonad
     -- ask before quitting my entire session
    , ((modm .|. shiftMask, xK_q), myConfirmPrompt myXPConfig "Would you like to quit?" $ io (exitWith ExitSuccess))

    -- workspace navigation --
    , ((modm, xK_Left), prevWS)
    , ((modm, xK_Right), nextWS)
    , ((modm .|. shiftMask, xK_Left), shiftToPrev)
    , ((modm .|. shiftMask, xK_Right), shiftToNext)
    , ((modm, xK_Down), toggleWS)

    -- multimedia keys --
    , ((0, xF86XK_AudioRaiseVolume), spawn "amixer -q sset Master 2%+" )
    , ((0, xF86XK_AudioLowerVolume), spawn "amixer -q sset Master 2%-" )
    , ((0, xF86XK_AudioMute), spawn "amixer set Master toggle" )
    , ((0, xF86XK_Calculator), spawn "qalculate-gtk")
    , ((0, xF86XK_Search), spawn runLauncher)
    , ((0, xF86XK_Mail), spawn mailClient)
    ]

{- My chosen layouts -}
myLayoutHook = tiled |||
               Mirror tiled |||
               noBorders (fullscreenFull Full) |||
               Grid
  where
     tiled   = Tall nmaster delta ratio
     nmaster = 1
     ratio   = 1/2
     delta   = 2/100

{- Special rules for certain programs -}
myManageHook =
    composeAll
    [ className =? "Gimp" --> doFloat
    , title =? "Qalculate!" --> doFloat
    ]

{- Tying it all together -}
main = do
    xmproc <- spawnPipe "xmobar $HOME/.xmobarrc"
    xmonad $ docks $ fullscreenSupport $ def
        { modMask            = mod4Mask -- use Super rather than Alt
        , borderWidth        = 2
        , startupHook = do
            spawn "xcompmgr"
            spawnHere "feh --bg-scale $HOME/.xmonad/background.png"
        , terminal           = "xterm"
        , normalBorderColor  = "#cccccc" -- grey
        , focusedBorderColor = "#8700ff" -- purple
        , focusFollowsMouse = False
        , keys = \conf -> myKeys conf `M.union` keys def conf
        , manageHook = manageDocks
            <+> insertPosition Below Newer
            <+> manageHook def
        , layoutHook =
            gaps [(U, 20), (D, 15), (R, 15), (L, 15)]
            $ spacingRaw True (Border 0 10 10 10) True
                (Border 10 10 10 10) True
            $ avoidStruts
            $ lessBorders OnlyScreenFloat
            $ myLayoutHook
        , logHook = dynamicLogWithPP xmobarPP
            { ppOutput = hPutStrLn xmproc
            , ppCurrent = xmobarColor "yellow" "" . wrap "[" "]"
            , ppHiddenNoWindows = xmobarColor "grey" ""
            , ppTitle = xmobarColor "yellow" "" . shorten 40
            , ppVisible = wrap "(" ")"
            , ppUrgent = xmobarColor "red" "yellow"
            }
        }

